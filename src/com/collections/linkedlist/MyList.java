package com.collections.linkedlist;

public class MyList {
    private Node node;
    private int size = 0;

    public MyList(){
        node = null;
    }
    public boolean isEmpty(){
        return node == null;
    }
    public int getSize(){
        return size;
    }
    public void addFirst (int value){
        Node newNode = new Node();
        newNode.setValue(value);

        if (isEmpty()){
            node = newNode;
        }else{
            newNode.setNext(node);
            node = newNode;
        }
        size++;
    }
    public void addEnd(int value){
        Node newNode = new Node();
        newNode.setValue(value);

        if (isEmpty()){
            node = newNode;
        }else{
            Node aux = node;

            while (aux.getNext() != null ){
                aux = aux.getNext();
            }
            aux.setNext(newNode);
        }
        size++;
    }
    public void insertReference(int reference, int value){
        Node newNode = new Node();
        newNode.setValue(value);

        if (!isEmpty()){
            if (search(reference)){
                Node aux = node;
                while (aux.getValue() != reference){
                    aux = aux.getNext();
                }

                Node next = aux.getNext();
                aux.setNext(newNode);
                newNode.setNext(next);
                size++;
            }
        }
    }
    public void insertRPosition(int position, int value){
        if (position >=0 && position <= size){
            Node newNode = new Node();
            newNode.setValue(value);

            if (position == 0){
                newNode.setNext(node);
                node = newNode;
            }else{
                if (position == size){
                    Node aux = node;

                    while (aux.getNext() != null){
                        aux = aux.getNext();
                    }
                    aux.setNext(newNode);
                }else{
                    Node aux = node;

                    for (int i=0; i < (position-1); i++){
                        aux = aux.getNext();
                    }
                    Node next = aux.getNext();
                    aux.setNext(newNode);
                    newNode.setNext(next);
                }
            }
            size++;
        }
    }
    public int getValue (int position) throws Exception{
        if (position >=0  && position <= size){
            if (position == 0){
                return node.getValue();
            }else{
                Node aux = node;

                for(int i = 0; i < position; i++){
                    aux = aux.getNext();
                }
                return aux.getValue();
            }
        }else{
            throw  new Exception("The position is not in the list");
        }
    }
    public boolean search (int reference){
        Node aux = node;
        boolean detected = false;

        while (aux != null && detected != true){
            if(reference == aux.getValue()){
                detected = true;
            }else{
                aux = aux.getNext();
            }
        }
        return detected;
    }
    public int getPosition(int reference) throws Exception{
        if (search(reference)){
            Node aux = node;
            int counter = 0;

            while (reference != aux.getValue()){
                counter++;
                aux = aux.getNext();
            }
            return counter;
        }else{
            throw new Exception("Value not found");
        }
    }
    public void modifyReference (int reference, int value){
        if (search(reference)){
            Node aux = node;

            while (aux.getValue() != reference){
                aux = aux.getNext();
            }
            aux.setValue(value);
        }
    }
    public void modifyPosition(int position, int value){
        if(position >=0  && position <= size){
            if (position == 0){
                node.setValue(value);
            }else{
                Node aux = node;

                for (int i = 0; i< position; i++){
                    aux = aux.getNext();
                }
                aux.setValue(value);
            }
        }
    }
    public void removeReference(int reference){
        if (search(reference)){
            if(node.getValue() == reference){
                node = node.getNext();
            }else{
                Node aux = node;

                while (aux.getNext().getValue() != reference){
                    aux = aux.getNext();
                }
                Node next = aux.getNext().getNext();
                aux.setNext(next);
            }
            size--;
        }
    }
    public void removePosisition(int position){
        if (position >=0  && position < size){
            if (position == 0){
                node = node.getNext();
            }else{
                Node aux = node;

                for (int i=0; i < position-1; i++){
                    aux = aux.getNext();
                }
                Node next = aux.getNext();
                aux.setNext(next.getNext());
            }
            size--;
        }
    }
    public void deleted(){
        node = null;
        size = 0;
    }
    public void show(){
        if (!isEmpty()){
            Node aux = node;
            int i = 0;

            while (aux != null){
                System.out.println( i + ".[ " + aux.getValue() + " ]");
                aux = aux.getNext();
                i++;
            }
        }
    }
}
