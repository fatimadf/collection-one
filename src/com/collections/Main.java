package com.collections;

import com.collections.linkedlist.MyList;
import com.collections.queue.MyQueue;
import com.collections.stack.MyStack;

public class Main {
    public static void main (String [] args) throws Exception{
        MyList lista = new MyList();

        System.out.println("<<-- Ejemplo de lista simple -->>\n");

        // Agregar al final de la lista
        lista.addEnd(12);
        lista.addEnd(15);
        lista.addEnd(9);
        // Agregar in inicio de la lista
        lista.addFirst(41);
        lista.addFirst(6);

        System.out.println("<<-- Lista -->>");
        lista.show();

        System.out.println("\n\n<<-- Tamaño -->>");
        System.out.println(lista.getSize());

        System.out.println("\n<<-- Obtener el valor del nodo en la posicion 3 -->>");
        System.out.println(lista.getValue(3));

        System.out.println("\nInsrta un nodo con valor 16 despues del 15");
        lista.insertReference(15, 16);
        lista.show();
        System.out.print("Tamaño: ");
        System.out.println(lista.getSize());

        System.out.println("\n\nInsrta un nodo con valor 44 en la posición 3");
        lista.insertRPosition(3, 44);
        lista.show();
        System.out.print("Tamaño: ");
        System.out.println(lista.getSize());

        System.out.println("\nActualiza el valor 12 del tercer nodo por 13");
        lista.modifyReference(12, 13);
        lista.show();
        System.out.print("Tamaño: ");
        System.out.println(lista.getSize());

        System.out.println("\nActualiza el valor nodo en la posición 0 por 17");
        lista.modifyPosition(0, 17);
        lista.show();
        System.out.print("Tamaño: ");
        System.out.println(lista.getSize());

        System.out.println("\nElimina el nodo con el valor 41");
        lista.removeReference(41);
        lista.show();
        System.out.print("Tamaño: ");
        System.out.println(lista.getSize());

        System.out.println("\nElimina el nodo en la posición 2");
        lista.removePosisition(2);
        lista.show();
        System.out.print("Tamaño: ");
        System.out.println(lista.getSize());

        System.out.println("\nConsulta si existe el valor 30");
        System.out.println(lista.search(30));

        System.out.println("\nConsulta la posicion del valor 9");
        System.out.println(lista.getPosition(9));

        System.out.println("\nElimina la lista");
        lista.deleted();

        System.out.println("\nConsulta si la lista está vacia");
        System.out.println(lista.isEmpty());

        System.out.println("\n\n<<-- Fin de ejemplo lista simple -->>");


        System.out.println("------------------------------------------------------------------");


        MyStack pila = new MyStack();

        System.out.println("<<-- Ejemplo de pila -->>\n");

        pila.add(2);
        pila.add(3);
        pila.add(4);
        pila.add(1);
        pila.add(7);

        System.out.println("<<-- Pila -->>");
        pila.show();
        System.out.print("Tamaño: " + pila.getSize());

        System.out.println("\n\nElimina el nodo que se encuentra en el tope de la pila");
        pila.deleteTop();
        pila.show();
        System.out.print("Tamaño: " + pila.getSize());

        System.out.println("\n\nActualiza el nodo que tiene el valor 3 por 10");
        pila.edit(3, 10);
        pila.show();

        System.out.println("\n\nConsulta si existe el valor 4");
        System.out.println(pila.search(4));

        System.out.println("\nElimina la pila");
        pila.delete();

        System.out.println("\n\nConsulta si la pila esta vacia");
        System.out.println(pila.isEmpty());

        System.out.println("\n\n<<-- Fin de ejemplo pila -->>");


        System.out.println("------------------------------------------------------------------");


        MyQueue cola = new MyQueue();

        System.out.println("<<-- Ejemplo de cola -->>\n");

        cola.add(7);
        cola.add(3);
        cola.add(2);
        cola.add(5);
        cola.add(4);

        System.out.println("<<-- Cola -->>");
        cola.show();
        System.out.print("\nTamaño: " + cola.getSize());

        System.out.println("\n\nElimina el primer nodo que ingreso a la cola");
        cola.remove();
        cola.show();
        System.out.print("\nTamaño: " + cola.getSize());

        System.out.println("\n\nElimina la cola");
        cola.deleted();

        System.out.println("\n\nConsulta si la cola esta vacia");
        System.out.println(cola.isEmpty());

        System.out.println("\n\n<<-- Fin de ejemplo cola -->>");

    }
}
