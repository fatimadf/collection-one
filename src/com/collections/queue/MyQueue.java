package com.collections.queue;

public class MyQueue {
    private Node init;
    private Node end;
    private int size = 0;

    public MyQueue(){
        init = null;
        end = null;
    }
    public void add (int value){
        Node n = new Node(value);
        n.setNext(null);

        if (init == null && end == null){
            init = n;
            end = n;
        }
        end.setNext(n);
        end = end.getNext();
        size++;
    }
    public void remove(){
        int value = init.getValue();
        init = init.getNext();
        size--;
    }
    public boolean isEmpty(){
        return init == null && end == null;
    }
    public int getSize(){
        return size;
    }
    public void deleted(){
        init = null;
        end = null;
        size = 0;
    }
    public void show(){
        Node n = this.init;

        while (n!=null){
            System.out.print( n.getValue() + "  ");
            n = n.getNext();
        }
    }
}
