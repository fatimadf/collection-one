package com.collections.stack;

public class MyStack {
    private Node node;
    private int size = 0;

    public MyStack(){
        node = null;
    }
    public boolean isEmpty(){
        return node == null;
    }
    public int getSize(){
        return size;
    }
    public void add(int value){
        Node newNode = new Node();
        newNode.setValue(value);

        if (isEmpty()){
            node = newNode;
        }else{
            newNode.setNext(node);
            node = newNode;
        }
        size++;
    }
    public void deleteTop(){
        if (!isEmpty()){
            node = node.getNext();
            size--;
        }
    }
    public int valueTop() throws Exception{
        if (!isEmpty()){
            return node.getValue();
        }else{
            throw new Exception("Stack is empty");
        }
    }
    public boolean search(int reference){
        Node aux = node;
        boolean detected = false;

        while (detected != true && aux != null){
            if (reference == aux.getValue()){
                detected = true;
            }else {
                aux = aux.getNext();
            }
        }
        return detected;
    }
    public void deleteValue (int reference){
        if (search(reference)){
            Node aux = null;

            while (reference != node.getValue()){
                Node nodeTemp = new Node();
                nodeTemp.setValue(node.getValue());
                if (aux == null){
                    aux = nodeTemp;
                }else{
                    nodeTemp.setNext(aux);
                    aux = nodeTemp;
                }
                deleteTop();
            }
            deleteTop();
            while (aux != null){
                add(aux.getValue());
                aux = aux.getNext();
            }
            aux = null;
        }
    }
    public void edit (int reference, int value){
        if (search(reference)){
            Node aux = null;

            while (reference != node.getValue()){
                Node nodeTemp = new Node();
                nodeTemp.setValue(node.getValue());

                if (aux == null){
                    aux = nodeTemp;
                }else{
                    nodeTemp.setNext(aux);
                    aux = nodeTemp;
                }
                deleteTop();
            }
            node.setValue(value);

            while (aux != null){
                add(aux.getValue());
                aux = aux.getNext();
            }
            aux = null;
        }
    }
    public void delete (){
        node = null;
        size = 0;
    }
    public void show(){
        Node aux = node;

        while(aux != null){
            System.out.println("|\t" + aux.getValue() + "\t|");
            System.out.println("---------");
            aux = aux.getNext();
        }
    }
}
